import requests
from behave import given, when, then
from hamcrest import assert_that, equal_to


@given(u'I set REST API url: "{api_url}"')
def step_impl(context, api_url):
    context.api_url = api_url


@when(u'I add two numbers: number_1="{number_1}",number_2="{number_2}"')
def step_impl(context, number_1, number_2):
    d = {"number_1": number_1, "number_2": number_2}
    response = requests.put(url=context.api_url, json=d)
    assert_that(response.status_code, equal_to(200), "status code response")
    context.input = (number_1, number_2)
    context.result = response.json()


@then(u'The sum should be equal to: expected_result="{expected_result:d}"')
def step_impl(context, expected_result):
    assert_that(context.result, equal_to(expected_result), "sum operation result")
