Feature: Check SUM operation

  Check that the sum operation behaces correctly and returns the sum of 2 numbers

Scenario: Get the sum of 2 numbers
        Given I set REST API url: "http://127.0.0.1:5000/api/operation/sum"
        When I add two numbers: number_1="2",number_2="1"
        Then The sum should be equal to: expected_result="3"