from flask import Flask
from flask_restful import Resource, Api
from flask_restful import reqparse

parser = reqparse.RequestParser()

app = Flask(__name__)
api = Api(app)

todos = {}
from leap_beyond.core.operation import Operation


class OperationResource(Resource):
    """
    Available operations

    """

    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('number_1', type=float, help='should be a number')
        self.parser.add_argument('number_2', type=float, help='should be a number')
        self.available_operations = {
            "sum": Operation.sum
        }

    def put(self, operation):
        try:
            op = self.available_operations[operation]
            args = self.parser.parse_args()
            return op(args['number_1'], args['number_2'])
        except KeyError:
            return


api.add_resource(OperationResource, '/api/operation/<string:operation>')

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
