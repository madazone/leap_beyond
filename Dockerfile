# Image de base
FROM python:3.7.3-slim-stretch

RUN apt update
RUN apt install -y python3-dev gcc

RUN pip3.7 install --upgrade pip
RUN mkdir /leap_beyond
ENV PYTHONPATH "${PYTHONPATH}:/leap_beyond"

COPY . /leap_beyond

RUN pip3.7 install --default-timeout=100 -r /leap_beyond/requirements.txt

WORKDIR /leap_beyond
ENTRYPOINT ["/usr/local/bin/python3", "leap_beyond/bin/server.py"]