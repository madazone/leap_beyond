Requirements
============
* Python v3.7

Installation
============
Install PIP requirements
> pip install -r requirements.txt

Run the server in DEV mode
> python bin/server.py

Run the server in PROD with Docker
> docker run -d -p 80:5000 madazone/leap_beyond:latest

Check installation
> curl http://0.0.0.0:80/api/operation/sum -d "number_1=1"  -d "number_2=18" -X PUT

Testing
============
Change directory
> cd leap_beyond/test

Run behave test
> behave behave test/features/